// $Id: ActionInitialization.hh 11.12.2018 A Fijalkowska $
//
/// \file ActionInitialization.hh
/// \brief Class that initialize basic user classes:
/// PrimaryGeneratorAction, RunAction, EventAction and SteppingAction 

#ifndef ActionInitialization_h
#define ActionInitialization_h 1

#include "G4VUserActionInitialization.hh"

class ActionInitialization : public G4VUserActionInitialization
{
  public:
    ActionInitialization();
    virtual ~ActionInitialization();
    virtual void BuildForMaster() const;
    virtual void Build() const;
};

#endif
