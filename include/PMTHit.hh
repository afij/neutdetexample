
// $Id: PMTHit.hh 12.08.2016 A. Fijalkowska $
//
/// \file PMTHit.hh
/// \brief Definition of the PMTHit class
//
//
#ifndef PMTHit_h
#define PMTHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4LogicalVolume.hh"

class PMTHit : public G4VHit
{
  public:
 
    PMTHit(G4int moduleIndexVal);
    virtual ~PMTHit();
    PMTHit(const PMTHit &right);

    const PMTHit& operator=(const PMTHit &right);
    G4int operator==(const PMTHit &right) const;

    inline void *operator new(size_t);
    inline void operator delete(void *aHit);
    
    void AddHit();
    inline G4int GetModuleIndex() { return moduleIndex; }
	inline G4int GetPhotonsCounts() {return hitsNr;}
	
	
  private:
    G4int hitsNr;
    G4int moduleIndex;

};

typedef G4THitsCollection<PMTHit> PMTHitsCollection;

extern G4ThreadLocal G4Allocator<PMTHit>* PMTHitAllocator;

inline void* PMTHit::operator new(size_t){
  if(!PMTHitAllocator)
      PMTHitAllocator = new G4Allocator<PMTHit>;
  return (void *) PMTHitAllocator->MallocSingle();
}

inline void PMTHit::operator delete(void *aHit){
  PMTHitAllocator->FreeSingle((PMTHit*) aHit);
}

#endif

