// $Id: AnalysisManager.hh 12.08.2016 A. Fijalkowska $
//
/// \file AnalysisManager.hh
/// \brief Definition of the AnalysisManager singleton class
/// It accumulates statistic of detected optical photons

#ifndef AnalysisManager_h
#define AnalysisManager_h 1

#include "G4RootAnalysisManager.hh"
#include "G4String.hh"
#include "PMTHit.hh"

class AnalysisManager
{
	public:

		void CreateOutput(G4String filename);
		void SaveOutput();
		void AddNrOfEvent(G4int nrOfEvents);
		void AddHit(PMTHitsCollection* pmtHC, G4int eventId);
		
	private:
		AnalysisManager();
		virtual ~AnalysisManager() {}
		void CreateTuple();
		void CreatePMTTuple();
		void CreateNrOfCountsTuple();
		
		static AnalysisManager *s_instance;
		G4RootAnalysisManager* rootManager;
		G4int nrOfCreatedTuple;		
		G4int optPhTupleId;
		G4int nrOrCountsTupleId;
		
		
	public:
	static AnalysisManager* GetInstance()
	{
		if (!s_instance)
			s_instance = new AnalysisManager();
		return s_instance;
	}
		
};

#endif
