// $Id: NeutDet.hh 11.12.2018 A Fijalkowska $
//
/// \file NeutDet.hh
/// \brief Definition of the VANDLEBar class
//
#ifndef NeutDet_H
#define NeutDet_H 1

#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "MaterialsManager.hh"
#include "G4RotationMatrix.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "PMTSD.hh"

class NeutDet
{
  public:
    NeutDet();
    virtual ~NeutDet();

    virtual void ConstructSDandField();
    void Place(G4RotationMatrix *pRot, 
               const G4ThreeVector &tlate, 
               const G4String &pName, 
               G4LogicalVolume *pMotherLogical, 
               G4int pCopyNo);
   void Place(const G4Transform3D& transform3D, 
              const G4String &pName, 
              G4LogicalVolume *pMotherLogical, 
              G4int pCopyNo);	
	
  private:
  
	void SetBasicSizes();	                     
    void MakeWrapping();
    void MakePlasticDet();
    void MakePMTPhotocathode();
    
    void SetOpticalSurfacesProperties();
    
    MaterialsManager* materialsManager;
    
    G4double cylinderRadius;
	G4double cylinderHeigh;
	G4double wrappingThickness;
	G4double photocathodeThickness;
	G4double photocathodeRadius;
	
	G4LogicalVolume* wrappingLog;
	G4LogicalVolume* plasticLog;
	G4LogicalVolume* photocathodeLog;

    static PMTSD* pmtSD;
    
};

#endif							 
