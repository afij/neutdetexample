
// $Id: EventAction.cc 11.30.2016 A. Fijalkowska $
//
/// \file EventAction.cc
/// \brief Implementation of the EventAction class
//
//
#include "EventAction.hh"
#include "PMTHit.hh"
#include "AnalysisManager.hh"
#include "G4SDManager.hh"

EventAction::EventAction()
{
  pmtCollID = -1;
}
 
EventAction::~EventAction(){}


void EventAction::BeginOfEventAction(const G4Event* anEvent)
{
  currEventId = anEvent->GetEventID();
  //New event, add the user information object
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if(pmtCollID < 0)
    pmtCollID=SDman->GetCollectionID("pmtHitCollection");
}
 

void EventAction::EndOfEventAction(const G4Event* anEvent)
{
	
  G4int eventID = anEvent->GetEventID();
// periodic printing every 100 events
  if( eventID % 100 == 0 )
  {
    G4cout << "Finished Running Event # " << eventID << G4endl;
  }
  
  
  //sensitive detectores
  PMTHitsCollection* pmtHC = 0;
  G4HCofThisEvent* hitsCE = anEvent->GetHCofThisEvent();
  //Get the hit collections
  if(hitsCE)
  {
    if(pmtCollID >= 0)
       pmtHC = (PMTHitsCollection*)( hitsCE->GetHC(pmtCollID) );
  }

   AnalysisManager* analysisManager = AnalysisManager::GetInstance();
  if(pmtHC)
  {
      analysisManager->AddHit(pmtHC, eventID);
  }
	
}

int EventAction::currEventId = 0;
