
// $Id: AnalysisManager.cc 12.16.2016 A. Fijalkowska $
//
/// \file AnalysisManager.cc
/// \brief Definition of the AnalysisManager singleton class
//
//
#include "AnalysisManager.hh"
#include "G4SystemOfUnits.hh"
AnalysisManager::AnalysisManager()
{
   rootManager = G4RootAnalysisManager::Instance();	
   rootManager->SetVerboseLevel(1);
   rootManager->SetFirstHistoId(0);
   rootManager->SetFirstNtupleId(0);
   rootManager->SetFirstNtupleColumnId(0);  
   nrOfCreatedTuple = 0;

}

void AnalysisManager::CreateOutput(G4String filename)
{
  rootManager->OpenFile(filename);
  CreateTuple();   
}


void AnalysisManager::SaveOutput()
{
  rootManager->Write();
  rootManager->CloseFile();	
}

void AnalysisManager::CreateTuple()
{
  CreatePMTTuple();
  CreateNrOfCountsTuple();
}

void AnalysisManager::CreatePMTTuple()
{
  rootManager->CreateNtuple("PMTHitInfo", "Detected photons");
  rootManager->CreateNtupleIColumn("eventID");//D - double, I - int
  rootManager->CreateNtupleIColumn("detID");
  rootManager->CreateNtupleIColumn("nrOfPhotons");
  rootManager->FinishNtuple();
  optPhTupleId = nrOfCreatedTuple++;

//add first empty event
  int colId = 0;
  rootManager->FillNtupleIColumn(optPhTupleId, colId, 0); 
  rootManager->FillNtupleIColumn(optPhTupleId, ++colId, 0);
  rootManager->FillNtupleIColumn(optPhTupleId, ++colId, 0);
  rootManager->AddNtupleRow(optPhTupleId);
}


void AnalysisManager::CreateNrOfCountsTuple()
{
  rootManager->CreateNtuple("NrOfCountsInfo", "Tot nr of counts");
  rootManager->CreateNtupleIColumn("nrOfCounts");
  rootManager->FinishNtuple();
  nrOrCountsTupleId = nrOfCreatedTuple++;
}


void AnalysisManager::AddHit(PMTHitsCollection* pmtHC, G4int eventId)
{
  G4int pmts = pmtHC->entries();
  for(G4int i=0; i!=pmts; i++)
  {
    G4int moduleIndex = (*pmtHC)[i]->GetModuleIndex();
    G4int photonsCounts = (*pmtHC)[i]->GetPhotonsCounts();
	int colId = 0;
    rootManager->FillNtupleIColumn(optPhTupleId, colId, eventId); 
    rootManager->FillNtupleIColumn(optPhTupleId, ++colId, moduleIndex);
    rootManager->FillNtupleIColumn(optPhTupleId, ++colId, photonsCounts);
    rootManager->AddNtupleRow(optPhTupleId);
  }	
}

void AnalysisManager::AddNrOfEvent(G4int nrOfEvents)
{
    int colId = 0;
    rootManager->FillNtupleIColumn(nrOrCountsTupleId, colId, nrOfEvents); 	
    rootManager->AddNtupleRow(nrOrCountsTupleId);
}
	
AnalysisManager *AnalysisManager::s_instance = 0;
