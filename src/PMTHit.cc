// $Id: PMTHit.cc 12.09.2016 A. Fijalkowska $
//
/// \file PMTHit.cc
/// \brief Implementation of the PMTHit class
//
//
#include "PMTHit.hh"
#include "G4ios.hh"
#include "G4VVisManager.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4VPhysicalVolume.hh"
#include "Exception.hh"

G4ThreadLocal G4Allocator<PMTHit>* PMTHitAllocator=0;


PMTHit::PMTHit(G4int moduleIndexVal):moduleIndex(moduleIndexVal)
{
	hitsNr = 1;
}                   


PMTHit::~PMTHit() {}


PMTHit::PMTHit(const PMTHit &right) : G4VHit()
{ 
   hitsNr = right.hitsNr;
   moduleIndex = right.moduleIndex;
}

const PMTHit& PMTHit::operator=(const PMTHit &right)
{
   hitsNr = right.hitsNr;
   moduleIndex = right.moduleIndex;
   return *this;
}

G4int PMTHit::operator==(const PMTHit &right) const
{
  if(hitsNr==right.hitsNr && moduleIndex==right.moduleIndex)
      return 1;
  return 0;
}
	
void PMTHit::AddHit()
{
	hitsNr ++;	
}
