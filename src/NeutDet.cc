// $Id: VANDLEBar.cc 17.10.2016 A Fijalkowska $
//
/// \file VANDLEBar.cc
/// \brief Implementation of the VANDLEBar class based on 
///  optical/LXe/src/LXeMainVolume.cc
//
//
#include "NeutDet.hh"

#include "globals.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4Material.hh"
#include "G4OpticalSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4SystemOfUnits.hh"
#include "G4VisAttributes.hh"
#include "G4SDManager.hh"

NeutDet::NeutDet()                
{
  SetBasicSizes();
  MakeWrapping();
  MakePlasticDet();
  MakePMTPhotocathode();	
  SetOpticalSurfacesProperties();	
}

NeutDet::~NeutDet() {}

void NeutDet::SetBasicSizes()
{
//rozmiar sa oczywiscie do zmiany
    cylinderRadius = 5*cm;
    cylinderHeigh = 10*cm;
	wrappingThickness = 0.1*mm;	
	photocathodeThickness = 1.0*mm;
	photocathodeRadius = 1*cm;
	                             
}
	

void NeutDet::MakeWrapping()
{
  G4double innerRadius = 0.0*mm;
  G4double startPhi = 0.0 * degree;
  G4double deltaPhi = 360.0 * degree;
  G4Tubs* wrappingSolid = new G4Tubs("wrappingSolid", 
                                  innerRadius, 
                                  cylinderRadius + wrappingThickness, 
                                  cylinderHeigh + 2*wrappingThickness, 
                                  startPhi, 
                                  deltaPhi);
  materialsManager = MaterialsManager::GetInstance();
  G4Material* aluminum = materialsManager->GetAluminum();
  wrappingLog = new G4LogicalVolume(wrappingSolid, aluminum, "wrappingLog");

  G4VisAttributes* visAtt = new G4VisAttributes( G4Colour(0.,0.,1.) );
  visAtt->SetLineWidth(0.2);
  visAtt->SetForceAuxEdgeVisible(true);
  wrappingLog->SetVisAttributes(visAtt);
} 

							 
void NeutDet::MakePlasticDet()
{
  G4double innerRadius = 0.0*mm;
  G4double startPhi = 0.0 * degree;
  G4double deltaPhi = 360.0 * degree;
  materialsManager = MaterialsManager::GetInstance();	
  G4Tubs* plasticSolid = new G4Tubs("plasticSolid", 
                                 innerRadius, 
                                 cylinderRadius, 
                                 cylinderHeigh, 
                                 startPhi, 
                                 deltaPhi);
                 
  G4Material* BC408 = materialsManager->GetBC408();
  plasticLog = new G4LogicalVolume(plasticSolid, BC408, "plasticLog");


  G4VisAttributes* visAtt = new G4VisAttributes( G4Colour(1.,0.,0.) );
  visAtt->SetLineWidth(0.1);
  visAtt->SetForceAuxEdgeVisible(true);
  visAtt->SetForceSolid(true);
  plasticLog->SetVisAttributes(visAtt);
  G4ThreeVector pos = G4ThreeVector(0., 0., 0.);
  new G4PVPlacement( 0, pos, plasticLog, "plasticPhys", wrappingLog, 0, 0 );
}
    							 


void NeutDet::MakePMTPhotocathode()
{
  G4double innerRadius = 0.0*mm;
  G4double startPhi = 0.0 * degree;
  G4double deltaPhi = 360.0 * degree;
  materialsManager = MaterialsManager::GetInstance();	
  G4Tubs* photocathodeSolid = new G4Tubs("photocathodeSolid", 
                                 innerRadius, 
                                 photocathodeRadius, 
                                 photocathodeThickness, 
                                 startPhi, 
                                 deltaPhi);              
  G4Material* bialkali = materialsManager->GetBialkali();
  photocathodeLog = new G4LogicalVolume(photocathodeSolid, bialkali, "photocathodeLog");


  G4VisAttributes* visAtt = new G4VisAttributes( G4Colour(1.,1.,0.) );
  visAtt->SetLineWidth(0.1);
  visAtt->SetForceAuxEdgeVisible(true);
  visAtt->SetForceSolid(true);
  photocathodeLog->SetVisAttributes(visAtt);
  G4ThreeVector pos = G4ThreeVector(0., 0., cylinderHeigh - photocathodeThickness);
  new G4PVPlacement( 0, pos, photocathodeLog, "photocathodePhys", plasticLog, 0, 0 );	
}
    

void NeutDet::SetOpticalSurfacesProperties()
{

	G4double photonsEn [] = {2.38*eV, 2.48*eV, 2.58*eV, 2.69*eV,
                             2.75*eV, 2.82*eV, 2.92*eV, 2.95*eV, 
                             3.02*eV, 3.10*eV, 3.26*eV, 3.44*eV};
    int scintEntries = 12;
                             
    //**Photocathode surface properties                         
    G4double photocathEfficiency[]={1., 1., 1., 1., 1., 1.,
		                            1., 1., 1., 1., 1., 1.};   
    G4double photocathReflectivity[]={0., 0., 0., 0., 0., 0.,
		                              0., 0., 0., 0., 0., 0.};
       
    G4MaterialPropertiesTable* photocathMatTable = new G4MaterialPropertiesTable();
    photocathMatTable->AddProperty("EFFICIENCY", 
                                    photonsEn,
                                    photocathEfficiency, 
                                    scintEntries);
    photocathMatTable->AddProperty("REFLECTIVITY", 
                                    photonsEn,
                                    photocathReflectivity, 
                                    scintEntries);
    
    G4OpticalSurface* photocathOpSurf = new G4OpticalSurface("photocathOpSurf");
    photocathOpSurf->SetModel( unified );
    photocathOpSurf->SetFinish( polished );
    photocathOpSurf->SetType( dielectric_metal );		   		
    photocathOpSurf->SetMaterialPropertiesTable(photocathMatTable);
    new G4LogicalSkinSurface( "photocathOptSkin", photocathodeLog, photocathOpSurf);


     
     //** reflective foil properties		                            
	G4double foilEfficiency[] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
		                          0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    G4double foilReflectivity[] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 
		                            1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
		                            
    // rIndex of Al taken from 
    // http://refractiveindex.info/?shelf=main&book=Al&page=Rakic                            
    // G4double foilRIndex[] = { 0.86, 0.79, 0.72, 0.65, 0.62, 0.58, 
	//	                      0.54, 0.52, 0.50, 0.47, 0.41, 0.37 };
		                      
	G4double plasticWallSpecularLobe[] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
		                                   0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	G4double plasticWallSecularSpike[] = { 0.99, 0.99, 0.99, 0.99,
		                                   0.99, 0.99, 0.99, 0.99, 
		                                   0.99, 0.99, 0.99, 0.99};
		                                   
	G4double plasticWallBackscatter[] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
		                                  0.0, 0.0, 0.0, 0.0, 0.0, 0.0};	                          
    G4MaterialPropertiesTable* reflectFoilMatTable = new G4MaterialPropertiesTable();
	reflectFoilMatTable->AddProperty( "EFFICIENCY", 
	                                   photonsEn, 
	                                   foilEfficiency, 
	                                   scintEntries );
	reflectFoilMatTable->AddProperty( "REFLECTIVITY", 
	                                   photonsEn, 
	                                   foilReflectivity, 
	                                   scintEntries );
	reflectFoilMatTable->AddProperty( "SPECULARLOBECONSTANT", 
	                                   photonsEn, 
	                                   plasticWallSpecularLobe, 
	                                   scintEntries );
	reflectFoilMatTable->AddProperty( "SPECULARSPIKECONSTANT", 
	                                   photonsEn, 
	                                   plasticWallSecularSpike, 
	                                   scintEntries );
	reflectFoilMatTable->AddProperty( "BACKSCATTERCONSTANT", 
	                                   photonsEn, 
	                                   plasticWallBackscatter, 
	                                   scintEntries );	
	G4OpticalSurface* reflectFoilOpSurf = new G4OpticalSurface("reflectFoilOpSurf");
	reflectFoilOpSurf->SetType( dielectric_metal );
	reflectFoilOpSurf->SetFinish( polished );
	reflectFoilOpSurf->SetModel( unified );
	reflectFoilOpSurf->SetMaterialPropertiesTable(reflectFoilMatTable);
	new G4LogicalSkinSurface( "reflectFoilSkin", wrappingLog, reflectFoilOpSurf);

}


void NeutDet::ConstructSDandField()
{
  // PMT SD
  if (!pmtSD) 
  {
    //Created here so it exists as pmts are being placed
    G4cout << "Construction /NeutDet/pmtSD" << G4endl;
    pmtSD = new PMTSD("/NeutDet/pmtSD");
    pmtSD->SetModuleDeph(2); 
  }

  //sensitive detector is not actually on the photocathode.
  //processHits gets done manually by the stepping action.
  //It is used to detect when photons hit and get absorbed&detected at the
  //boundary to the photocathode (which doesnt get done by attaching it to a
  //logical volume.
  //It does however need to be attached to something or else it doesnt get
  //reset at the begining of events   
   G4SDManager* SDman = G4SDManager::GetSDMpointer();
   SDman->AddNewDetector(pmtSD);
   photocathodeLog->SetSensitiveDetector(pmtSD);

	
}

void NeutDet::Place(G4RotationMatrix *pRot, 
                      const G4ThreeVector &tlate, 
                      const G4String &pName, 
                      G4LogicalVolume *pMotherLogical, 
                      G4int pCopyNo)
{
   new G4PVPlacement(pRot, 
                     tlate, 
                     wrappingLog, 
                     pName, 
	                 pMotherLogical,
	                 0,
	                 pCopyNo);					  
						  
						  
}


void NeutDet::Place(const G4Transform3D& transform3D, 
                      const G4String &pName, 
                      G4LogicalVolume *pMotherLogical, 
                      G4int pCopyNo)
{
   new G4PVPlacement(transform3D,
		             wrappingLog,
		             pName, 
	                 pMotherLogical,
	                 0,
	                 pCopyNo);	 	
}

    
PMTSD* NeutDet::pmtSD = 0;

